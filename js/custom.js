jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    //Sticky Header
    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();

        if (scroll >= 200) {
            jQuery(".header").addClass("fixed-header");
        } else {
            jQuery(".header").removeClass("fixed-header");
        }
    });
    
    // Client Logo Slider
    $('.logo-slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    
    // Testimonial Slider
    $('.testimonial-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });
    
});

 //Preloader
 jQuery(window).on('load',function () {
    jQuery('.preloader').remove();
});
    
//Paraleex

  if(navigator.userAgent.match(/Trident\/7\./)) {
  document.body.addEventListener("mousewheel", function() {
    event.preventDefault();
    var wd = event.wheelDelta;
    var csp = window.pageYOffset;
    window.scrollTo(0, csp - wd);
  });
}

jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap, textarea').on('keypress', function (event) {
        var jQuerythis = jQuery(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

// Header Scroll

jQuery(document).ready(function () {
    jQuery(document).on("scroll", onScroll);
    //smoothscroll
    jQuery('.navbar-nav li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        jQuery(document).off("scroll");
        jQuery('.navbar-nav li a').each(function () {
            jQuery(this).removeClass('active');
        })
        jQuery(this).addClass('active');
        var target = this.hash
            , menu = target;
        target = jQuery(target);
        jQuery('html, body').stop().animate({
            'scrollTop': target.offset().top + 2
        }, 1000, 'swing', function () {
            window.location.hash = target;
            jQuery(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event) {
    var scrollPos = jQuery(document).scrollTop();
    jQuery('.navbar-nav li a').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            jQuery('.navbar-nav li a').removeClass("active");
            currLink.addClass("active");
        }
        else {
            currLink.removeClass("active");
        }
    });
}

document.querySelector("html").classList.add('js');

var fileInput  = document.querySelector( ".input-file" ),  
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");
      
button.addEventListener( "keydown", function( event ) {  
    if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        fileInput.focus();  
    }  
});
button.addEventListener( "click", function( event ) {
   fileInput.focus();
   return false;
});  
fileInput.addEventListener( "change", function( event ) {  
    the_return.innerHTML = this.value;  
});  